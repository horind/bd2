package exercicio;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Administrativo;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Cidade;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Endereco;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Estado;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Funcionario;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.PessoaJuridica;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.RamoAtividade;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Situacao;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Vendedor;

public class Exercicio {

	public static void main(String[] args) throws ParseException {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("exerciciojpa01");

		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		massaDeDados(em);

		em.getTransaction().commit();

		cidadeQuantidadeVendedor(em);
		clientePorVendedor(em, "Ezequias");
		ramosAtividade(em, "Calcado");
		funcionariosAtivos(em, Situacao.ATIVO);
		estadosSemCidades(em);
		vendedoresCidade(em, "Feira de Santana");

		em.close();
		emf.close();

	}

	private static void estadosSemCidades(EntityManager em) {
		// Estados sem cidade
		// Filtro: (nenhum)
		// Resultado: sigla e nome dos estados que não possuem cidades
		// cadastradas
		TypedQuery<Estado> estadoSemCidade = em.createQuery("select e from Estado e where size (e.cidades) = 0",
				Estado.class);
		List<Estado> estadosSemCidade = estadoSemCidade.getResultList();

		System.out.println("--------------------------------------------------------------------");
		for (Estado estado : estadosSemCidade) {
			System.out.println(estado.getSigla() + " " + estado.getNome());
		}
	}

	private static void vendedoresCidade(EntityManager em, String nomeCidade) {
		// 6 - Vendedores de uma cidade
		// Filtro: nome da cidade
		// Resultado: nome e telefone do vendedor
		TypedQuery<Vendedor> vendedoresCidade = em
				.createQuery("select v from Vendedor v where v.endereco.cidade.nome = :cidade", Vendedor.class);
		vendedoresCidade.setParameter("cidade", nomeCidade);
		List<Vendedor> funcResult = vendedoresCidade.getResultList();

		System.out.println("--------------------------------------------------------------------");
		for (Funcionario funcionario : funcResult) {
			System.out.println(funcionario.getNome() + " | " + funcionario.getTelefone());
		}
	}

	private static void funcionariosAtivos(EntityManager em, Situacao situacao) {
		// Funcionarios ativos
		// Filtro: (nenhum)
		// Resultado: cpf e nome dos funcionários que têm situação ATIVO
		TypedQuery<Funcionario> funcionariosAtivos = em
				.createQuery("select f from Funcionario f where f.situacao = :situacao", Funcionario.class);
		funcionariosAtivos.setParameter("situacao", situacao);
		List<Funcionario> listAtivos = funcionariosAtivos.getResultList();

		System.out.println("--------------------------------------------------------------------");
		for (Funcionario funcAtivos : listAtivos) {
			System.out.println(funcAtivos.getCpf() + " | " + funcAtivos.getNome());
		}
	}

	private static void ramosAtividade(EntityManager em, String nomeRamoAtividade) {
		// 2 - Ramos de atividade
		// Filtro: nome do ramo atividade
		// Resultado: nome, telefone e endereço dos vendedores que atendem à
		// clientes desse ramo de atividade
		Query query = em.createNativeQuery("select " + "f.nome, " + "f.telefone, " + "f.bairro, " + "f.logradouro, "
				+ "f.cidade_sigla " + "from vendedor v " + "inner join  tab_funcionario f on v.cpf = f.cpf "
				+ "inner join vendedor_cliente vc on vc.vendedor = f.cpf "
				+ "inner join pessoajuridica_ramosatividade pjr on pjr.cnpj = vc.cliente "
				+ "inner join ramoatividade r on pjr.ramo_id = r.id " + "where r.nome = :nomeRamoAtividade");
		query.setParameter("nomeRamoAtividade", nomeRamoAtividade);
		List<Object[]> ramosAtividade = query.getResultList();
		System.out.println("--------------------------------------------------------------------");
		for (Object[] objects : ramosAtividade) {
			System.out.println(
					objects[0] + " | " + objects[1] + " | " + objects[2] + " | " + objects[3] + " | " + objects[4]);
		}
	}

	private static void clientePorVendedor(EntityManager em, String nomeVendedor) {
		// 1 - Clientes por vendedor
		// Filtro: nome vendedor
		// Resultado: nome e endereço dos clientes desse vendedor
		Query clientePorVendedor = em
				.createNativeQuery("select cliente.nome,cliente.bairro, cliente.logradouro, cliente.cidade_sigla"
						+ " from pessoajuridica cliente "
						+ "inner join vendedor_cliente vc on vc.cliente = cliente.cnpj"
						+ " inner join vendedor v on v.cpf = vc.vendedor "
						+ "inner join tab_funcionario f on v.cpf = f.cpf where f.nome = :nomeVendedor");
		clientePorVendedor.setParameter("nomeVendedor", nomeVendedor);
		List<Object[]> clientePorVendedorResult = clientePorVendedor.getResultList();
		System.out.println("--------------------------------------------------------------------");
		for (Object[] objects : clientePorVendedorResult) {
			System.out.println(objects[0] + " | " + objects[1] + " | " + objects[2] + " | " + objects[3]);
		}
	}

	private static void cidadeQuantidadeVendedor(EntityManager em) {
		// Cidade e quantidade de Clientes nessa cidade
		// Filtro: (nenhum)
		// Resultado: nome da cidade e quantidade de Clientes dessa cidade

		Query qtdPessoaJuridica = em
				.createNativeQuery("select cidade.nome, count(pessoajuridica.nome) as num_pj from  cidade cidade"
						+ " inner join pessoajuridica on pessoajuridica.cidade_sigla = cidade.sigla group by cidade.nome");
		List<Object[]> pessoasJuridicasPorCidades = qtdPessoaJuridica.getResultList();
		System.out.println("--------------------------------------------------------------------");
		for (Object[] objects : pessoasJuridicasPorCidades) {
			System.out.println(objects[0] + " | " + objects[1]);
		}

	}

	private static void massaDeDados(EntityManager em) throws ParseException {
		Estado bahia = new Estado();
		bahia.setSigla("BA");
		bahia.setNome("Bahia");
		em.persist(bahia);

		Estado sergipe = new Estado();
		sergipe.setSigla("SE");
		sergipe.setNome("Sergipe");
		em.persist(sergipe);

		Estado saopaulo = new Estado();
		saopaulo.setSigla("SP");
		saopaulo.setNome("Sao Paulo");
		em.persist(saopaulo);

		Cidade saopaulocp = new Cidade();
		saopaulocp.setNome("Sao paulo");
		saopaulocp.setSigla("SP");
		saopaulocp.setEstado(saopaulo);
		em.persist(saopaulocp);

		Cidade voltaredonda = new Cidade();
		voltaredonda.setNome("Volta Redonda");
		voltaredonda.setSigla("VR");
		voltaredonda.setEstado(saopaulo);
		em.persist(voltaredonda);

		Cidade feiradesantana = new Cidade();
		feiradesantana.setNome("Feira de Santana");
		feiradesantana.setSigla("FS");
		feiradesantana.setEstado(bahia);
		em.persist(feiradesantana);

		Cidade canudos = new Cidade();
		canudos.setNome("Canudos");
		canudos.setSigla("CN");
		canudos.setEstado(bahia);
		em.persist(canudos);

		Cidade camacari = new Cidade();
		camacari.setNome("Camacari");
		camacari.setSigla("CM");
		camacari.setEstado(bahia);
		em.persist(camacari);

		List<Cidade> cidadesba = new ArrayList<Cidade>();
		cidadesba.add(feiradesantana);
		cidadesba.add(camacari);
		cidadesba.add(canudos);
		bahia.setCidades(cidadesba);

		Endereco endereco = new Endereco();
		endereco.setBairro("Ab");
		endereco.setLogradouro("Rua A");
		endereco.setCidade(feiradesantana);

		List<Cidade> cidadessp = new ArrayList<Cidade>();
		cidadessp.add(voltaredonda);
		cidadessp.add(saopaulocp);
		saopaulo.setCidades(cidadessp);

		RamoAtividade transporte = new RamoAtividade();
		transporte.setNome("Transporte");
		em.persist(transporte);

		RamoAtividade calcado = new RamoAtividade();
		calcado.setNome("Calcado");
		em.persist(calcado);

		RamoAtividade vestuario = new RamoAtividade();
		vestuario.setNome("Vestuario");
		em.persist(vestuario);

		PessoaJuridica lielson = new PessoaJuridica();
		lielson.setCnpj("16.421.188/0001-81");
		lielson.setNome("Lielson");
		lielson.setEndereco(endereco);
		lielson.setFaturamento(new BigDecimal(10.5));
		em.persist(lielson);

		PessoaJuridica matheus = new PessoaJuridica();
		matheus.setCnpj("53.359.757/0001-32");
		matheus.setNome("Matheus");
		matheus.setEndereco(endereco);
		matheus.setFaturamento(new BigDecimal(21.0));
		em.persist(matheus);

		PessoaJuridica pedro = new PessoaJuridica();
		pedro.setCnpj("04.202.625/0001-97");
		pedro.setNome("Pedro");
		pedro.setEndereco(endereco);
		pedro.setFaturamento(new BigDecimal(42.0));
		em.persist(pedro);

		PessoaJuridica maria = new PessoaJuridica();
		maria.setCnpj("72.206.384/0001-04");
		maria.setNome("Maria");
		maria.setEndereco(endereco);
		maria.setFaturamento(new BigDecimal(84.0));
		em.persist(maria);

		PessoaJuridica joao = new PessoaJuridica();
		joao.setCnpj("71.741.425/0001-08");
		joao.setNome("Joao");
		joao.setEndereco(endereco);
		joao.setFaturamento(new BigDecimal(168.0));
		em.persist(joao);

		List<RamoAtividade> ramoTransport = new ArrayList<RamoAtividade>();
		ramoTransport.add(transporte);
		matheus.setRamosAtividade(ramoTransport);
		lielson.setRamosAtividade(ramoTransport);

		List<RamoAtividade> ramovestuario = new ArrayList<RamoAtividade>();
		ramovestuario.add(vestuario);
		ramovestuario.add(calcado);
		pedro.setRamosAtividade(ramovestuario);
		lielson.setRamosAtividade(ramovestuario);
		maria.setRamosAtividade(ramovestuario);

		SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");

		Vendedor filipe = new Vendedor();
		filipe.setCpf("785.642.467-24");
		filipe.setDataNascimento(data.parse("22/07/1995"));
		filipe.setEndereco(endereco);
		filipe.setNome("Filipe");
		filipe.setTelefone("147");
		filipe.setPercentualComissao(new BigDecimal(5.0));
		filipe.setSituacao(Situacao.ATIVO);
		em.persist(filipe);

		Vendedor gabriela = new Vendedor();
		gabriela.setCpf("071.895.585-43");
		gabriela.setDataNascimento(data.parse("22/08/1991"));
		gabriela.setEndereco(endereco);
		gabriela.setNome("Gabriela");
		gabriela.setTelefone("542");
		gabriela.setPercentualComissao(new BigDecimal(10.0));
		gabriela.setSituacao(Situacao.ATIVO);
		em.persist(gabriela);

		Vendedor ezequias = new Vendedor();
		ezequias.setCpf("071.895.585-44");
		ezequias.setDataNascimento(data.parse("01/06/1993"));
		ezequias.setEndereco(endereco);
		ezequias.setNome("Ezequias");
		ezequias.setTelefone("641");
		ezequias.setPercentualComissao(new BigDecimal(15.0));
		ezequias.setSituacao(Situacao.SUSPENSO);
		em.persist(ezequias);

		Vendedor mariana = new Vendedor();
		mariana.setCpf("488.464.370-47");
		mariana.setDataNascimento(data.parse("23/12/1994"));
		mariana.setEndereco(endereco);
		mariana.setNome("Mariana");
		mariana.setTelefone("421");
		mariana.setPercentualComissao(new BigDecimal(20.0));
		mariana.setSituacao(Situacao.SUSPENSO);
		em.persist(mariana);

		Vendedor marilene = new Vendedor();
		marilene.setCpf("132.720.958-61");
		marilene.setDataNascimento(data.parse("15/02/1997"));
		marilene.setEndereco(endereco);
		marilene.setNome("Marilene");
		marilene.setTelefone("849");
		marilene.setPercentualComissao(new BigDecimal(25.0));
		marilene.setSituacao(Situacao.ATIVO);
		em.persist(marilene);

		List<PessoaJuridica> clientes = new ArrayList<>();
		clientes.add(pedro);
		clientes.add(matheus);
		filipe.setClientes(clientes);
		gabriela.setClientes(clientes);

		List<Vendedor> vendedores = new ArrayList<>();
		vendedores.add(ezequias);
		vendedores.add(gabriela);
		lielson.setVendedores(vendedores);

		List<Vendedor> vendedores2 = new ArrayList<>();
		vendedores2.add(marilene);
		vendedores2.add(mariana);
		joao.setVendedores(vendedores2);

		Administrativo fernando = new Administrativo();
		fernando.setCpf("1527");
		fernando.setDataNascimento(data.parse("05/04/1995"));
		fernando.setEndereco(endereco);
		fernando.setNome("Fernando");
		fernando.setTelefone("467");
		fernando.setTurno(11);
		em.persist(fernando);

		Administrativo rogerio = new Administrativo();
		rogerio.setCpf("7459");
		rogerio.setDataNascimento(data.parse("30/09/1981"));
		rogerio.setEndereco(endereco);
		rogerio.setNome("Rogerio");
		rogerio.setTelefone("899");
		rogerio.setTurno(9);
		em.persist(rogerio);

		Administrativo juan = new Administrativo();
		juan.setCpf("6239");
		juan.setDataNascimento(data.parse("20/08/1988"));
		juan.setEndereco(endereco);
		juan.setNome("Juan");
		juan.setTelefone("467");
		juan.setTurno(11);
		em.persist(juan);
	}

}
