package br.ucsal.bes20172.bd2.exerciciojpa01.converters;

import javax.persistence.AttributeConverter;

import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Situacao;

public class SituacaoConverter implements AttributeConverter<Situacao, String> {

	@Override
	public String convertToDatabaseColumn(Situacao situacao) {
		if (Situacao.ATIVO.equals(situacao)) {
			return "A";
		}
		if (Situacao.SUSPENSO.equals(situacao)) {
			return "S";
		}
		return null;
	}

	@Override
	public Situacao convertToEntityAttribute(String dbData) {
		if ("A".equals(dbData)) {
			return Situacao.ATIVO;
		}
		if ("S".equals(dbData)) {
			return Situacao.SUSPENSO;
		}
		return null;
	}

}
