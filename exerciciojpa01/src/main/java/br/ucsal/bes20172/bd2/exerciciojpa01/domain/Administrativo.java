package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
// @DiscriminatorValue("A")
public class Administrativo extends Funcionario {

	@Column(nullable = false)
	Integer turno;

	public Integer getTurno() {
		return turno;
	}

	public void setTurno(Integer turno) {
		this.turno = turno;
	}

}
